<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->post('login', 'AuthController@authenticate');
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    //user login
    $router->post('login', 'AuthController@authenticate');

    //register user
    $router->post('register','usersController@register');

    //upload file
    $router->post('upload','episodeController@upload');

    $router->group(
        ['middleware' => 'jwt.auth'],
        function () use ($router) {
            //all shows
            $router->get('shows/all','showController@allShows');

            //selected show
            $router->get('shows/selected/{id}','showController@selectedShow');

            //check show follow
            $router->get('users/show/{id}','usersController@userCheckShowFollow');

            //check episode like
            $router->get('users/episode/{id}','usersController@userCheckEpisodeLike');

            //like episode
            $router->put('users/episode/{id}','usersController@likeEpisode');

            //like episode
            $router->put('users/dislike/episode/{id}','usersController@disLikeEpisode');

            //episodes data
            $router->get('episodes/{id}','episodeController@getEpisodeData');

            //latest episodes
            $router->get('latest','showController@latestShow');

            //random episodes
            $router->get('shows/menu','showController@menuShows');

            $router->group(['prefix' => 'users'], function () use ($router) {
                //get user information
                $router->get('info', 'usersController@userInfo');
            });

            //admin APIs
            $router->group(['prefix' => 'admin'], function () use ($router) {
                $router->group(
                    ['middleware' => 'adminAuth'],
                    function () use ($router) {


                        $router->group(['prefix' => 'shows'], function () use ($router) {

                            $router->post('add','showController@addShow');
                            $router->delete('delete/{id}','showController@deleteShow');
                            $router->put('edit/{id}','showController@editShow');

                        });

                        $router->group(['prefix' => 'episodes'], function () use ($router) {

                            $router->post('add','episodeController@addEpsoide');
                            $router->delete('delete/{id}','episodeController@deleteEpisode');
                            $router->put('edit','episodeController@editEpisode');
                        });
                    });
            });
        });
});