<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *N
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ala',
            'email' => 'alaa.sh.1984@gmail.com',
            'role' => 'admin',
            'password' => password_hash('P@ssw0rd',PASSWORD_BCRYPT),
            'follow' => '[]',
            'likes' => '[]'
        ]);
    }
}
