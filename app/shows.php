<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class shows extends Model
{

    protected $table = 'shows';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'desc', 'show_time','episodes'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}