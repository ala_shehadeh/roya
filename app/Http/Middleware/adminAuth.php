<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\services\userServices;

class adminAuth
{
    private $userServices;
    public function __construct(userServices $userServices)
    {
        $this->userServices = $userServices;
    }

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->userServices->userInfo();

        if ($user->role != 'admin') {
            return response()->json(['error'=>'Not Authorised'],404);
        }

        return $next($request);
    }

}