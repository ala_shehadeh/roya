<?php
/**
 * Created by PhpStorm.
 * User: alaa.shehadeh
 * Date: 6/12/2018
 * Time: 10:09 AM
 */

namespace App\Http\traits;


trait GeneralTrait
{
    public function fileName() {
        $name = date('Gisuv');
        $name .= rand(1000,9999);
        return $name;
    }
    public function getFileExtention($name) {
        $name = explode('.',$name);
        return end($name);
    }
}