<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 10/5/2018
 * Time: 10:16 AM
 */

namespace App\Http\helper;
use Dirape\Token\Token;


class showHelper
{
    public static function prepareCreate($data) {
        $output = array();
        $output['title'] = $data->title;
        $output['desc'] = $data->description;
        $show = array('days'=>showHelper::daysPrepare($data->days),'time'=>$data->time);
        $output['show_time'] = json_encode($show);
        $output['episodes'] = json_encode(array());
        return $output;
    }
    public static function daysPrepare($days) {
        $output = array();
        $days = array_unique($days);
        foreach ($days as $value)
            $output[] = $value;
        return $output;
    }
    public static function prepareUpdate($data) {
        $output = array();
        $output['title'] = $data['showName'];
        $output['desc'] = $data['showDescription'];
        $output['show_time'] = json_encode($data['showTime']);
        $output['episodes'] = json_encode($data['episodes']);
        return $output;
    }
    public static function showOutput($data) {
        $output = array();
        $output['id'] = $data->id;
        $output['showName'] = $data->title;
        $output['showDescription'] = $data->desc;
        $output['showTime'] = json_decode($data->show_time);
        $output['episodes'] = json_decode($data->episodes);
        return $output;
    }
    public static function multiShowOutput($data) {
        $output = array();
        foreach ($data as $value)
            $output[] = showHelper::showOutput($value);
        return $output;
    }
    public static function checkDays($selected) {
        if(!is_array($selected) || count($selected) == 0)
            return false;
        else {
            $days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
            foreach ($selected as $day) {
                if (!in_array($day, $days))
                    return false;
            }
            return true;
        }
    }
    public static function addEpisode($data) {
        $output = array();
        $token = new Token();
        $output['uid'] = $token->random(6);
        $output['title'] = $data->title;
        $output['desc'] = $data->descripition;
        $output['duration'] = $data->duration;
        $output['time'] = $data->time;
        $output['thumbnail'] = $data->thumbnail;
        $output['url'] = $data->url;
        return $output;
    }
    public static function editEpisode($data) {
        $output['uid'] = $data->uid;
        $output['title'] = $data->title;
        $output['desc'] = $data->descripition;
        $output['duration'] = $data->duration;
        $output['time'] = $data->time;
        $output['thumbnail'] = $data->thumbnail;
        $output['url'] = $data->url;
        return $output;
    }
    public static function episodeOutput($data) {
        $output = array();
        $data = json_decode($data->episodes);
        foreach ($data as $value) {
            $output[$value->uid]['uid'] = $value->uid;
            $output[$value->uid]['url'] = $value->url;
            $output[$value->uid]['desc'] = $value->desc;
            $output[$value->uid]['time'] = $value->time;
            $output[$value->uid]['title'] = $value->title;
            $output[$value->uid]['duration'] = $value->duration;
            $output[$value->uid]['thumbnail'] = $value->thumbnail;
            $output[$value->uid]['youtube_key'] = showHelper::youtubeId($value->url);
            }
        return $output;
    }
    public static function episodeInput($data) {
        $output = array();
        foreach ($data as $key=>$value)
            $output[] = $value;
        return $output;
    }
    public static function urlCheck($url) {
        $url_parsed_arr = parse_url($url);
        if ($url_parsed_arr['host'] == "www.youtube.com" && $url_parsed_arr['path'] == "/watch" && substr($url_parsed_arr['query'], 0, 2) == "v=" && substr($url_parsed_arr['query'], 2) != "") {
            return true;
        } else {
            return false;
        }
    }
    public static function latestShow($data) {
        $output = array();
        foreach ($data as $key=>$value) {
            $show = showHelper::showOutput($value);
            $episodes = (array)$show['episodes'];
            $output[] = end($episodes);
        }
        return $output;
    }
    public static function youtubeId($url) {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        return $my_array_of_vars['v'];
    }
}