<?php
/**
 * Created by PhpStorm.
 * User: alaa.shehadeh
 * Date: 7/9/2018
 * Time: 9:50 AM
 */

namespace App\Http\Helpers;

class userHelper
{
    public static function userOutput($data) {
        $output = array();
        $output['id'] = $data->id;
        $output['name'] = $data->name;
        $output['email_address'] = $data->email;
        $output['role']= $data->role;
        $output['image'] = $data->profile;
        return $output;
    }
    public static function prepareCreate($data) {
        $output = array();
        $output['name'] = $data->name;

        if(@$data->role)
            $output['role'] = $data->role;
        else
            $output['role'] = 'user';

        $output['email'] = $data->email;
        $output['password'] = password_hash($data['password'],PASSWORD_BCRYPT);
        $output['image'] = $data->profile;
        $output['likes']= '[]';
        $output['follow'] = '[]';
        return $output;
    }
}