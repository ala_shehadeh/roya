<?php

namespace App\Http\Controllers;

use App\Http\Helpers\userHelper;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Repositories\usersRepository;
use Firebase\JWT\JWT;

class AuthController extends BaseController
{
    private $userRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(usersRepository $usersRepository)
    {
        $this->userRepository = $usersRepository;
    }
    protected function jwt($user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user['id'], // Subject of the token
            'info' => $user,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60*24 // Expiration time

        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }
    public function authenticate(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'address' => 'required|email',
            'pwd' => 'required'
        ]);
        $validator = $validator->messages();
        if (count($validator->all()) > 0) {
            return response()->json($validator->all(), 404);
        }

        $user = $this->userRepository->findByField(['email' => $request->address])->first();

        if ($user) {
            $userInfo = userHelper::userOutput($user);

            // Verify the password and generate the token
            if (password_verify($request->pwd, $user['password'])) {
                    return response()->json([
                        'token' => $this->jwt($userInfo)
                    ], 200);
            } else
                return response()->json(array('wrong user name or password'), 404);
        }
        else
            return response()->json(array('wrong user name or password'), 404);
    }
}
