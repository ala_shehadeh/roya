<?php

namespace App\Http\Controllers;
use App\Http\Helpers\userHelper;
use Validator;
use Illuminate\Http\Request;
use App\Http\services\userServices;
use App\Http\Repositories\usersRepository;

class usersController extends Controller
{
    private $userServices;
    private $userRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(userServices $userServices,usersRepository $usersRepository)
    {
        $this->userServices = $userServices;
        $this->userRepository = $usersRepository;
    }
    public function userInfo() {
        $user = $this->userServices->userInfo();
        if($user)
            return response()->json($user);
        else
            return response()->json(array('error'=>'Not authorized'),404);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'=>'required|min:3',
            'email'=>'required|unique:users|max:255',
            'password'=> 'required',
            'profile'=>'required|min:5'
        ]);
        $validator = $validator->messages();
        if (count($validator->all()) > 0)
            return response()->json($validator->all(), 404);
        else {
            $data = $this->userServices->createUser($request);
            return response()->json($data);
        }
    }


    public function editUser(Request $request) {
        //check if user exist
        $user = $this->userServices->selectedUser($request->uid);
        if(!$user)
            return response()->json(['error'=>'user not exist'], 404);
        else {
            if($request->email == $user['email_address'])
                $validator = userHelper::editUserValidator();
            else
                $validator = userHelper::addUserValidator();

            $validator = Validator::make($request->all(),$validator);

            $validator = $validator->messages();
            if (count($validator->all()) > 0)
                return response()->json($validator->all(), 404);
            else {
                $this->userServices->userUpdate($user['id'],$request);
                return response()->json(['success'=>true]);
            }
        }
    }
    function userCheckShowFollow($id) {
        $user = $this->userServices->userInfo();
        $data = $this->userRepository->showFollow($id,$user->id);

        if(count($data) > 0)
            return response()->json(['success'=>true]);
        else
            return response()->json(['error'],404);
    }
    function userCheckEpisodeLike($id) {
        $user = $this->userServices->userInfo();
        $data = $this->userRepository->episodeLike($id,$user->id);
        if($data)
            return response()->json(['success'=>true]);
        else
            return response()->json(['error'],404);
    }
    function likeEpisode($id) {
        $user = $this->userServices->userInfo();
        $this->userRepository->userLikeEpisode($id,$user->id);
        return response()->json(['success'=>true]);
    }
    function disLikeEpisode($id) {
        $user = $this->userServices->userInfo();
        $this->userRepository->episodeDislike($user->id,$id);
        return response()->json(['success'=>true]);
    }
}
