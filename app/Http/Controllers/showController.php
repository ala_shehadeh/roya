<?php

namespace App\Http\Controllers;
use App\Http\helper\showHelper;
use Validator;
use Illuminate\Http\Request;
use App\Http\Repositories\showRepository;
use App\Http\services\showServices;

class showController extends Controller
{
    private $showRepository;
    private $showServices;
    public function __construct(showRepository $showRepository,showServices $showServices)
    {
        $this->showRepository = $showRepository;
        $this->showServices = $showServices;
    }
    public function addShow(Request $request) {
        if(!showHelper::checkDays($request->days))
            return response()->json(['select the right days name from the list'],404);
        else {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:3',
                'description' => 'required',
                'time' => 'required|date_format:H:i'
            ]);
            $validator = $validator->messages();
            if (count($validator->all()) > 0)
                return response()->json($validator->all(), 404);
            else {
                $data = showHelper::prepareCreate($request);
                $this->showRepository->create($data);
                return response()->json(['sucess' => true]);
            }
        }
    }
    public function deleteShow($id) {
        $show = $this->showServices->checkShowId($id);
        if(!$show)
            return response()->json(['the selected show not exist'],404);
        else {
            $this->showRepository->delete($id);
            return response()->json(['success' => true]);
        }
    }
    public function editShow(Request $request,$id)
    {
        $show = $this->showServices->checkShowId($id);
        if (!$show)
            return response()->json(['the selected show not exist'], 404);
        else {
            if (!showHelper::checkDays($request->days))
                return response()->json(['select the right days name from the list'], 404);
            else {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|min:3',
                    'description' => 'required',
                    'time' => 'required|date_format:H:i'
                ]);
                $validator = $validator->messages();
                if (count($validator->all()) > 0)
                    return response()->json($validator->all(), 404);
                else {
                    $data = showHelper::prepareCreate($request);
                    $this->showRepository->update($data,$id);
                    return response()->json(['success' => true]);
                }
            }
        }
    }
    public function menuShows() {
        $data = $this->showRepository->menuShows(5);
        $data = showHelper::multiShowOutput($data);
        return response()->json($data);
    }
    public function allShows() {
        $data = $this->showRepository->all();
        $data = showHelper::multiShowOutput($data);
        return response()->json($data);
    }
    public function selectedShow($id) {
        $data = $this->showRepository->find($id);
        $data = showHelper::showOutput($data);
        return $data;
    }
    public function latestShow() {
        $data = $this->showRepository->latestShows();
        $data = showHelper::latestShow($data);
        return response()->json($data);
    }
}
