<?php

namespace App\Http\Controllers;
use App\Http\helper\showHelper;
use Validator;
use Illuminate\Http\Request;
use App\Http\Repositories\showRepository;
use App\Http\services\showServices;
use App\Http\traits\GeneralTrait;

class episodeController extends Controller
{
    use GeneralTrait;
    private $showRepository;
    private $showServices;
    public function __construct(showRepository $showRepository,showServices $showServices)
    {
        $this->showRepository = $showRepository;
        $this->showServices = $showServices;
    }
    public function addEpsoide(Request $request) {
        $show = $this->showServices->checkShowId($request->show_id);
        if (!$show)
            return response()->json(['the selected show not exist'], 404);
        else {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:3',
                'descripition' => 'required',
                'time' => 'required|date_format:H:i',
                'duration' => 'required|date_format:H:i',
                'thumbnail' => 'required',
                'url' => 'required'
            ]);
            $validator = $validator->messages();
            if (count($validator->all()) > 0)
                return response()->json($validator->all(), 404);
            else {
                if(!showHelper::urlCheck($request->url))
                    return response()->json(['the show url is not valid, only youtube url allowed'],404);
                else {
                    $data = showHelper::addEpisode($request);
                    $show['episodes'][] = $data;
                    $show = showHelper::prepareUpdate($show);
                    $output = $this->showRepository->update($show,$request->show_id);
                    $output = showHelper::showOutput($output);
                    return response()->json($output);
                }
            }
        }
    }
    public function getEpisodeData($id) {
        $data = $this->showRepository->episodeData($id);
        $data = showHelper::episodeOutput($data);
        return response()->json($data[$id]);
    }
    public function deleteEpisode($id) {
        $data = $this->showRepository->episodeData($id);
        $episodes = showHelper::episodeOutput($data);
        unset($episodes[$id]);
        $episodes = showHelper::episodeInput($episodes);
        $episodes = json_encode($episodes);
        $this->showRepository->update(['episodes'=>$episodes],$data->id);
        return response()->json(array('success'=>true));
    }
    public function editEpisode(Request $request) {
        $data = $this->showRepository->episodeData($request->uid);
        $episodes = showHelper::episodeOutput($data);
        $episodes[$request->uid] = showHelper::editEpisode($request);
        $episodes = json_encode(showHelper::episodeInput($episodes));
        $this->showRepository->update(['episodes'=>$episodes],$data->id);
        return response()->json(array('success'=>true));
    }
    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'mimes:jpeg,jpg,png,gif|required|max:10000'
        ]);
        $validator = $validator->messages();
        if (count($validator->all()) > 0)
            return response()->json(['only images allowed'], 404);
        else {
            $file = $request->file;
            $output = array();
            $path = base_path('public/files/');

            $name = $file->getClientOriginalName();
            $new_name = $this->fileName() . '.' . $this->getFileExtention($name);
            $file->move($path, $name);
            rename($path . $name, $path . $new_name);
            $output = array('thumbnail' => $new_name);

            return response()->json($output);
        }
    }
}
