<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 7/9/2018
 * Time: 10:06 PM
 */

namespace App\Http\services;

use App\Http\Helpers\userHelper;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Repositories\usersRepository;

class userServices
{
    private $usersRepository;
    public function __construct(usersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function userInfo()
    {
        $token = JWTAuth::getToken();
        if (!$token)
            return false;
        else {
            $user = @JWTAuth::parseToken()->getPayload();
            if ($user) {
                return $user['info'];
            }
            else
                return false;
        }
    }
    public function allUsers() {
        $users = $this->usersRepository->all();
        $users = userHelper::multiUsers($users);
        return $users;
    }
    public function createUser($data) {
        $data = userHelper::prepareCreate($data);
        $data = $this->usersRepository->create($data);
        $data = userHelper::userOutput($data);
        return $data;
    }
    public function selectedUser($uid) {
        $user = $this->usersRepository->findWhere(['uid'=>$uid])->first();
        if($user) {
            $user = userHelper::userOutput($user);
            return $user;
        }
        else
            return false;
    }
    public function userUpdate($id,$data) {
        $data = userHelper::prepareAdminUpdate($data);
        $this->usersRepository->update($data,$id);
        return true;
    }
    public function ownerUsers() {
        $data = $this->usersRepository->findByField('role','owner');
        $data = userHelper::multiUsers($data);
        return $data;
    }
}