<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 10/5/2018
 * Time: 6:38 PM
 */

namespace App\Http\services;
use App\Http\helper\showHelper;
use App\Http\Repositories\showRepository;


class showServices
{
    private $showRepository;
    public function __construct(showRepository $showRepository)
    {
        $this->showRepository = $showRepository;
    }
    public function checkShowId($id) {
        $show = $this->showRepository->findWhere(['id'=>$id]);
        if(count($show) == 0)
            return false;
        else
            return showHelper::showOutput($show->first());
    }
}