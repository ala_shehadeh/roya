<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 10/4/2018
 * Time: 11:39 PM
 */

namespace App\Http\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class showRepository extends BaseRepository
{
    function model()
    {
        return "App\\shows";
    }
    function episodeData($id) {
        $data = $this->model->whereRaw("JSON_CONTAINS(episodes->'$[*].uid', '\"$id\"')")->first();
        return $data;
    }
    function menuShows($limit) {
        $data = $this->model->take($limit)->inRandomOrder()->get();
        return $data;
    }
    function latestShows() {
        $data = $this->model->take(12)->orderBy('id','desc')->get();
        return $data;
    }
}