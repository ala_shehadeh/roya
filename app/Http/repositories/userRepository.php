<?php
/**
 * Created by PhpStorm.
 * User: Alaa
 * Date: 10/4/2018
 * Time: 11:39 PM
 */

namespace App\Http\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Facades\DB;

class usersRepository extends BaseRepository
{
    function model()
    {
        return "App\\users";
    }
    function showFollow($show_id,$user_id) {
        $data = $this->model->whereRaw("JSON_SEARCH(follow,'all','$show_id')")->where('id',$user_id)->first();
        return $data;
    }
    function episodeLike($episode_id,$user_id) {
        $data = $this->model->whereRaw("JSON_SEARCH(likes,'all','$episode_id') is not NULL")->where('id',$user_id)->first();
        return $data;
    }
    function userLikeEpisode($episode_id,$user_id) {
        DB::statement(
            "UPDATE `users` SET 
            `likes` = JSON_ARRAY_APPEND ( likes, '$', '$episode_id' ) where id = $user_id");
    }
    function episodeDislike($user_id,$episode_id) {
        DB::statement(
            "UPDATE `users` SET 
            likes = JSON_REMOVE(
  likes, replace(json_search(likes, 'all', '$episode_id'), '\"', '')
) where id = $user_id");
    }
}