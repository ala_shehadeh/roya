The system is fully nosql driver built depends on the project requirement by Lumen for backend development
 
to run the application please do the following

* glone the repository
* through cmd open the repository folder and run the command 
###### composer update
* rename .env.example to .env, open it and fill your mysql connection information
* for jwt secret key run the following command
######php artisan jwt:generate
* run the following command to install the database
######php artisan migrate
* run the following command to create the admin user
###### php artisan db:seed
the admin user name: alaa.sh.1984@gmail.com

password: P@ssw0rd
* run the following command to start the server
######cd public
######php -S localhost:8000
the server will be ready on port 8000, if this port already used you can change it as you like but you have to change the global host variable at index.js in frontend repository


####System requirements
* php 7
* Composer
* Mysql Database V5.7.8 or higher
* GIT